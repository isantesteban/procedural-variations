ResourceManager = {

    activeRenderTargets: [],
    inactiveRenderTargets: [],
    boundRenderTargets: [],

    maxInactive: 7, // Max number of targets in the stack of inactive render targets
    timeLimit: 0.5, // Time (in seconds) before a render target is released
    checkTimeout: 0.1, // Time (in seconds) between each check of the state of the resources

    checkingResources: false,

    /*
     * Returns a render target
     */
    getRenderTarget: function (resolution = 1024) {
        let renderTarget;

        // Get render target
        if (this.inactiveRenderTargets.length === 0) {
            renderTarget = new THREE.WebGLRenderTarget(resolution, resolution);
        } else {
            renderTarget = this.inactiveRenderTargets.pop();
        }

        // Activate render target
        this.activeRenderTargets[renderTarget.uuid] = renderTarget;
        renderTarget.clock = new THREE.Clock();
        renderTarget.clock.start();

        if (!this.checkingResources) {
            this.checkResources();
            this.checkingResources = true;
        }

        // Return render target
        return renderTarget;
    },

    /*
     * Releases a render target
     */
    releaseRenderTarget: function (renderTarget) {
        delete this.activeRenderTargets[renderTarget.uuid];

        if (this.inactiveRenderTargets.length >= this.maxInactive) {
            renderTarget.dispose();
        } else {
            this.inactiveRenderTargets.push(renderTarget);
        }
    },

    /*
     * Binds the texture stored in a render target to a material
     */
    bindTexture: function (material, renderTarget) {
        let currentRenderTarget = this.boundRenderTargets[material.id];

        if (currentRenderTarget) {
            currentRenderTarget.isBound = false;
            this.releaseRenderTarget(currentRenderTarget);
        }

        material.map = renderTarget.texture;
        renderTarget.isBound = true;

        this.boundRenderTargets[material.id] = renderTarget;
    },

    /*
     * Periodic routine that releases unused render targets
     */
    checkResources: function () {
        let numActiveTargets = Object.keys(this.activeRenderTargets).length;

        for (let uuid in this.activeRenderTargets) {

            if (!this.activeRenderTargets.hasOwnProperty(uuid)) continue;

            let renderTarget = this.activeRenderTargets[uuid];
            let elapsedTime = renderTarget.clock.getElapsedTime();

            if (!renderTarget.isBound && elapsedTime > this.timeLimit) {
                this.releaseRenderTarget(renderTarget);
            }
        }

        if (numActiveTargets > 0) {
            setTimeout(this.checkResources.bind(this), this.checkTimeout * 1000);
        } else {
            this.checkingResources = false;
        }
    }

};

