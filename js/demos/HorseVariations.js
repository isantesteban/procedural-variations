let textures, customizers;
let coatColours = ['Bay', 'White', 'Grey', 'Dun'];

function initCustomizers() {
    customizers = [];

    initTextures();

    initCustomizerDefault();
    initCustomizerWhite();
    initCustomizerDun();
    initCustomizerGrey();
    initCustomizerBay();
}

function initTextures() {
    textures = [];

    let geometry = DemoSetup.resources['horse_geometry'].geometry;

    textures['body_mask'] = DemoSetup.resources['body_mask'];
    textures['tail_mask'] = DemoSetup.resources['tail_mask'];
    textures['head_mask'] = DemoSetup.resources['head_mask'];

    /* --------------------------------------------------------------------------------------*/

    textures['body_spots_1'] = new ProceduralTexture({
        geometry: geometry,
        mask: textures['body_mask'],
        frequency: 1.75,
        threshold: 0.85,
        sharpness: 10,
        max_z: 3.0,
        min_y: 2.5
    });

    Utilities.randomizeFloat(textures['body_spots_1'], 'displacement');

    textures['body_spots_2'] = new ProceduralTexture({
        geometry: geometry,
        mask: textures['body_mask'],
        frequency: 3.0,
        sharpness: 4.0,
        distortion: 2.0,
        max_z: 3.0,
        min_y: 2.5
    });

    Utilities.randomizeFloat(textures['body_spots_2'], 'displacement');
    Utilities.randomizeFloat(textures['body_spots_2'], 'max_z', 0.0, 3.0);
    Utilities.randomizeFloat(textures['body_spots_2'], 'threshold', 0.75, 0.85);

    textures['body_spots_3'] = new ProceduralTexture({
        geometry: geometry,
        mask: textures['body_mask'],
        frequency: 5.0,
        sharpness: 4.0,
        distortion: 2.0,
        max_z: 0.0,
        min_y: 2.5
    });

    Utilities.randomizeFloat(textures['body_spots_3'], 'displacement');
    Utilities.randomizeFloat(textures['body_spots_3'], 'max_z', 0.0, 3.0);
    Utilities.randomizeFloat(textures['body_spots_3'], 'threshold', 0.75, 0.85);

    textures['spots_reversed'] = new ProceduralTexture({
        geometry: geometry,
        mask: textures['body_mask'],
        frequency: 1.5,
        threshold: 0.3,
        sharpness: 12.0,
    });

    Utilities.randomizeFloat(textures['spots_reversed'], 'displacement');
    Utilities.randomizeFloat(textures['spots_reversed'], 'threshold', 0.0, 0.1);

    /* --------------------------------------------------------------------------------------*/

    textures['soft_snout'] = new ProceduralTexture({
        geometry: geometry,
        frequency: 0.01,
        min_z: 4.8,
        max_y: 5.7
    });

    Utilities.randomizeFloat(textures['soft_snout'], 'displacement');

    textures['soft_legs'] = new ProceduralTexture({
        geometry: geometry,
        mask: textures['body_mask'],
        frequency: 0.35,
        max_y: 3.0,
    });

    Utilities.randomizeFloat(textures['soft_legs'], 'displacement');
    Utilities.randomizeFloat(textures['soft_legs'], 'max_y', 2.0, 3.0);

    textures['soft_body'] = new ProceduralTexture({
        geometry: geometry,
        mask: textures['body_mask'],
        frequency: 0.3
    });

    Utilities.randomizeFloat(textures['soft_body'], 'displacement');

    /* --------------------------------------------------------------------------------------*/

    textures['head_mark'] = new ProceduralTexture({
        geometry: geometry,
        mask: textures['head_mask'],
        frequency: 0.25,
        frequency_x: 0.0,
        threshold: 0.55,
        sharpness: 15,
        max_x: 0.04,
        min_x: 0.00,
        min_z: 4.88
    });

    Utilities.randomizeFloat(textures['head_mark'], 'displacement', -2, 2);

    textures['leg_marks'] = new ProceduralTexture({
        geometry: geometry,
        mask: textures['body_mask'],
        frequency: 1.5,
        frequency_y: 0.0,
        sharpness: 5.0,
        distortion: 3.0,
    });

    Utilities.randomizeFloat(textures['leg_marks'], 'displacement', -2, 2);
    Utilities.randomizeFloat(textures['leg_marks'], 'max_y', 0, 1.5);

    /* --------------------------------------------------------------------------------------*/

    textures['spotted_blanket_1'] = new ProceduralTexture({
        geometry: geometry,
        mask: textures['body_mask'],
        threshold: 0.25,
        sharpness: 12.0,
        max_z: -0.4,
        distortion: 0.5
    });

    Utilities.randomizeFloat(textures['spotted_blanket_1'], 'displacement', -2, 2);
    Utilities.randomizeFloat(textures['spotted_blanket_1'], 'threshold', 0.2, 0.3);
    Utilities.randomizeFloat(textures['spotted_blanket_1'], 'max_z', -2.0, 0.0);

    textures['spotted_blanket_2'] = new ProceduralTexture({
        geometry: geometry,
        mask: textures['body_mask'],
        frequency: 1.2,
        threshold: 0.8,
        sharpness: 12.0,
        max_z: 1.7,
        min_y: 4.5,
        min_z: -0.40
    });

    Utilities.randomizeFloat(textures['spotted_blanket_2'], 'displacement', -2, 2);

    textures['spotted_blanket_3'] = new ProceduralTexture({
        geometry: geometry,
        mask: textures['body_mask'],
        frequency: 1.5,
        sharpness: 12.0,
        max_z: 0.5,
        max_y: 2.0
    });
}

function initCustomizerDefault() {
    customizers['Default'] = new ModelCustomizer();

    let parameters = {
        height: 0,
        weight: 0,
        tailSize: 0
    };

    // Tail modifier
    let tailModifier = new SkeletonModifier();
    let tailScale = new THREE.Vector3(1.0, 1.0, 1.0);

    tailModifier.scaleBone('tail.001', tailScale);
    tailModifier.scaleBone('tail.002', tailScale);

    Utilities.bindProperty(parameters, 'tailSize', tailScale, 'z', (x) => 1 + x);

    // Height modifier
    let heightModifier = new SkeletonModifier();

    let upperArmScale = new THREE.Vector3(1.0, 1.0, 1.0);
    heightModifier.scaleBone('upper_arm.L', upperArmScale);
    heightModifier.scaleBone('upper_arm.R', upperArmScale);

    let shinScale = new THREE.Vector3(1.0, 1.0, 1.0);
    heightModifier.scaleBone('shin.L', shinScale);
    heightModifier.scaleBone('shin.R', shinScale);

    let position = new THREE.Vector3(0.0, 0.0, 0.0);
    heightModifier.translateBone('root', position);

    Utilities.bindProperty(parameters, 'height', upperArmScale, 'y', (x) => 1.0 + 0.45 * x);
    Utilities.bindProperty(parameters, 'height', shinScale, 'y', (x) => 1.0 + x);
    Utilities.bindProperty(parameters, 'height', position, 'y');

    // Weight modifier
    let weightModifier = new SkeletonModifier();

    let bellyScale = new THREE.Vector3(1.0, 1.0, 1.0);
    weightModifier.scaleBone('belly', bellyScale);

    let thighScale = new THREE.Vector3(1.0, 1.0, 1.0);
    weightModifier.scaleBone('thigh.L', thighScale, false);
    weightModifier.scaleBone('thigh.R', thighScale, false);

    let neckScale = new THREE.Vector3(1.0, 1.0, 1.0);
    weightModifier.scaleBone('spine.004', neckScale, false);

    let forearmScale = new THREE.Vector3(1.0, 1.0, 1.0);
    weightModifier.scaleBone('forearm.L', forearmScale, false);
    weightModifier.scaleBone('forearm.R', forearmScale, false);

    Utilities.bindProperty(parameters, 'weight', bellyScale, 'y', (x) => 1 + 0.25 * x);
    Utilities.bindProperty(parameters, 'weight', thighScale, 'x', (x) => 1 + 0.25 * x);

    Utilities.bindProperty(parameters, 'weight', neckScale, 'x', (x) => 1 + 0.25 * x);
    Utilities.bindProperty(parameters, 'weight', neckScale, 'y', (x) => 1 + 0.25 * x);

    Utilities.bindProperty(parameters, 'weight', forearmScale, 'x', (x) => 1 + 0.5 * x);
    Utilities.bindProperty(parameters, 'weight', forearmScale, 'y', (x) => 1 + 0.5 * x);

    // Randomize height, weight and tail size
    Utilities.randomizeFloat(parameters, 'height', -0.1, 0.1);
    Utilities.randomizeFloat(parameters, 'weight', -0.2, 0.2);
    Utilities.randomizeFloat(parameters, 'tailSize', -0.2, 0.2);

    // Basic modifier that adds random noise to the horse's body
    let bodyNoise = new TextureModifier({
        masks: [textures['soft_body']],
    });

    Utilities.randomizeFloat(bodyNoise, 'lightness', -0.15, 0.15);

    customizers['Default'].addModifier(tailModifier);
    customizers['Default'].addModifier(heightModifier);
    customizers['Default'].addModifier(weightModifier);
    customizers['Default'].addModifier(bodyNoise);
}

function initCustomizerWhite() {
    customizers['White'] = customizers['Default'].clone();

    let hairColor = new TextureModifier({
        masks: [textures['body_mask'], textures['tail_mask']],
    });

    Utilities.randomizeFloat(hairColor, 'lightness', 0.8, 1.0);
    Utilities.randomizeFloat(hairColor, 'saturation', -0.35, -0.25);
    Utilities.randomizeBool(hairColor, 'enabled');

    let pointColoration = new TextureModifier({
        lightness: 1.0,
        masks: [textures['tail_mask'], textures['soft_snout'], textures['soft_legs']],
    });

    let bodySpots = new TextureModifier({
        masks: [textures['spots_reversed'], textures['tail_mask']]
    });

    Utilities.randomizeFloat(bodySpots, 'lightness', 0.8, 1.0);
    Utilities.randomizeFloat(bodySpots, 'saturation', -0.35, -0.25);
    Utilities.bindProperty(hairColor, '_enabled', bodySpots, 'enabled', (x) => !x);

    customizers['White'].addModifier(hairColor);
    customizers['White'].addModifier(pointColoration);
    customizers['White'].addModifier(bodySpots);
}

function initCustomizerDun() {
    customizers['Dun'] = customizers['Default'].clone();

    let hairColor = new TextureModifier({
        masks: [textures['body_mask'], textures['tail_mask']],
    });

    Utilities.randomizeFloat(hairColor, 'lightness', -0.2, 0.7);
    Utilities.randomizeFloat(hairColor, 'saturation', -0.15, -0.1);

    let pointColoration = new TextureModifier({
        lightness: -0.5,
        saturation: -0.2,
        masks: [textures['tail_mask'], textures['leg_marks']],
    });

    let softShadow = new TextureModifier({
        lightness: -1.5,
        saturation: -0.1,
        masks: [textures['soft_legs']],
    });

    let headMark = new TextureModifier({
        lightness: 0.8,
        saturation: -1.0,
        masks: [textures['head_mark']],
    });

    Utilities.randomizeBool(headMark, 'enabled');

    let legMarks = new TextureModifier({
        lightness: 4.0,
        saturation: -1.0,
        masks: [textures['leg_marks']],
    });

    Utilities.bindProperty(hairColor, '_lightness', legMarks, 'lightness', (x) => 4.5 - x);
    Utilities.randomizeBool(legMarks, 'enabled', 0.75);

    customizers['Dun'].addModifier(hairColor);
    customizers['Dun'].addModifier(softShadow);
    customizers['Dun'].addModifier(pointColoration);
    customizers['Dun'].addModifier(legMarks);
    customizers['Dun'].addModifier(headMark);
}

function initCustomizerGrey() {
    customizers['Grey'] = customizers['Default'].clone();

    let hairColor = new TextureModifier({
        saturation: -1,
        masks: [textures['body_mask'], textures['tail_mask']],
    });

    Utilities.randomizeFloat(hairColor, 'lightness', -0.3, 0.8);

    let bodySpots = new TextureModifier({
        saturation: -1,
        masks: [textures['body_spots_1'], textures['body_spots_2'], textures['body_spots_3']],
    });

    Utilities.randomizeFloat(bodySpots, 'lightness', -0.6, 0.6);
    Utilities.randomizeBool(bodySpots, 'enabled', 0.75);

    let pointColoration = new TextureModifier({
        lightness: -0.6,
        masks: [textures['soft_snout'], textures['soft_legs']],
    });

    let headMark = new TextureModifier({
        lightness: 0.8,
        saturation: -1.0,
        masks: [textures['head_mark']],
    });

    Utilities.randomizeBool(headMark, 'enabled');

    let legMarks = new TextureModifier({
        saturation: -1.0,
        masks: [textures['leg_marks']],
    });

    Utilities.bindProperty(hairColor, '_lightness', legMarks, 'lightness', (x) => 3.0 - x);
    Utilities.randomizeBool(legMarks, 'enabled');

    let spottedBlanket = new TextureModifier({
        saturation: -1.0,
        masks: [textures['spotted_blanket_1'], textures['spotted_blanket_2'], textures['spotted_blanket_3']],
    });

    Utilities.bindProperty(hairColor, '_lightness', spottedBlanket, 'lightness', (x) => 1.2 - x);
    Utilities.bindProperty(bodySpots, '_enabled', spottedBlanket, 'enabled', (x) => !x);

    customizers['Grey'].addModifier(hairColor);
    customizers['Grey'].addModifier(bodySpots);
    customizers['Grey'].addModifier(pointColoration);
    customizers['Grey'].addModifier(headMark);
    customizers['Grey'].addModifier(legMarks);
    customizers['Grey'].addModifier(spottedBlanket);
}

function initCustomizerBay() {
    customizers['Bay'] = customizers['Default'].clone();

    let hairColor = new TextureModifier({
        masks: [textures['body_mask'], textures['tail_mask']],
    });

    Utilities.randomizeFloat(hairColor, 'lightness', -0.8, 0.3);
    Utilities.randomizeFloat(hairColor, 'saturation', -0.08, 0.05);

    let headMark = new TextureModifier({
        saturation: -1.0,
        masks: [textures['head_mark']],
    });

    Utilities.randomizeBool(headMark, 'enabled');
    Utilities.bindProperty(hairColor, '_lightness', headMark, 'lightness', (x) => 1.2 - x);

    let legMarks = new TextureModifier({
        saturation: -1.0,
        masks: [textures['leg_marks']],
    });

    Utilities.randomizeBool(legMarks, 'enabled', 0.75);
    Utilities.bindProperty(hairColor, '_lightness', legMarks, 'lightness', (x) => 3.5 - x);

    let pointColoration = new TextureModifier({
        lightness: -0.5,
        saturation: -0.2,
        masks: [textures['soft_snout'], textures['soft_legs']],
    });

    Utilities.randomizeBool(pointColoration, 'enabled', 0.815);

    let spottedBlanket = new TextureModifier({
        saturation: -1.0,
        masks: [textures['spotted_blanket_1'], textures['spotted_blanket_2'], textures['spotted_blanket_3']],
    });

    Utilities.bindProperty(hairColor, '_lightness', spottedBlanket, 'lightness', (x) => 1.5 - x);
    Utilities.bindProperty(pointColoration, '_enabled', spottedBlanket, 'enabled', (x) => !x);

    let bodySpots = new TextureModifier({
        saturation: -1,
        masks: [textures['body_spots_1']],
    });

    Utilities.randomizeFloat(bodySpots, 'lightness', 0.7, 0.7);
    Utilities.bindProperty(hairColor, '_lightness', bodySpots, 'lightness', (x) => 1.5 - x);
    Utilities.bindProperty(spottedBlanket, 'enabled', bodySpots, 'enabled', (x) => !x && (Utilities.random() > 0.75));

    customizers['Bay'].addModifier(hairColor);
    customizers['Bay'].addModifier(legMarks);
    customizers['Bay'].addModifier(headMark);
    customizers['Bay'].addModifier(pointColoration);
    customizers['Bay'].addModifier(spottedBlanket);
    customizers['Bay'].addModifier(bodySpots);
}