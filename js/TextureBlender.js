class TextureBlender {

    constructor() {
        this.resolution = 1024;
    }

    blend(textures) {
        if (textures.length === 0) return null;
        if (textures.length === 1) return textures[0];

        // Make a copy of the original array to leave it unchanged
        let texturesToBlend = textures.slice();

        // Get the number of available texture units
        let numTexUnits = GraphicUtilities.getCapabilities().maxTextures;

        // Get the render targets used to blend the textures
        let firstRenderTarget = ResourceManager.getRenderTarget(this.resolution);
        let secondRenderTarget = ResourceManager.getRenderTarget(this.resolution);
        let currentRenderTarget = firstRenderTarget;

        while (texturesToBlend.length > 1) {

            // Select the textures to blend
            let iterationTextures = texturesToBlend.splice(0, numTexUnits);

            // Update the blending material with the selected textures
            this.updateMaterial(iterationTextures);

            // Blend the textures
            GraphicUtilities.renderQuad(this.textureBlendingMaterial, currentRenderTarget);

            // Add the result to the beginning of the array
            texturesToBlend.unshift(currentRenderTarget.texture);

            // Swap render targets for the next iteration
            if (currentRenderTarget.uuid === firstRenderTarget.uuid) {
                currentRenderTarget = secondRenderTarget;
            } else {
                currentRenderTarget = firstRenderTarget;
            }
        }

        return texturesToBlend[0];
    }

    updateMaterial(textures) {
        if (!this.textureBlendingMaterial) this.initMaterial();

        let uniforms = {
            textures: {type: 'tv', value: []}
        };

        for (let texture of textures) {
            uniforms.textures.value.push(texture);
        }

        this.textureBlendingMaterial.uniforms = uniforms;
        this.textureBlendingMaterial.defines['NUM_TEXTURES'] = uniforms.textures.value.length;
        this.textureBlendingMaterial.needsUpdate = true;
    }

    initMaterial() {
        this.textureBlendingMaterial = new THREE.ShaderMaterial({
            vertexShader: CustomShaders['basic_vert'],
            fragmentShader: CustomShaders['texture_blending_frag']
        });
    }

}