class ModelCustomizer {

    constructor() {
        this.modifiers = [];
        this.hasTextureModifiers = false;
        this.hasSkeletonModifiers = false;
    }

    clone() {
        let clone = new ModelCustomizer();
        clone.modifiers = this.modifiers.slice();
        return clone;
    }

    addModifier(modifier) {
        if (!modifier instanceof Modifier) {
            console.error("ModelCustomizer: invalid modifier");
            return;
        }

        if (modifier instanceof TextureModifier) {
            this.hasTextureModifiers = true;
        }

        if (modifier instanceof SkeletonModifier) {
            this.hasSkeletonModifiers = true;
        }

        this.modifiers.push(modifier);
    }

    clear() {
        this.modifiers = [];
        this.hasTextureModifiers = false;
        this.hasSkeletonModifiers = false;
    }

    customize(target, material, root) {
        if (!this.isValid(target, material)) return;

        // Get material
        if (!material) {
            if (!target.material.isMultiMaterial) material = target.material;
            else material = target.material.materials[0];
        }

        // Get root bone
        if (target.isSkinnedMesh) {
            root = target.skeleton.bones[0];
        }

        // Apply modifiers
        for (let modifier of this.modifiers) {

            if (!modifier.enabled) continue;

            if (modifier instanceof TextureModifier) {
                modifier.apply(material);
            } else if (modifier instanceof SkeletonModifier) {
                modifier.apply(root)
            } else {
                modifier.apply(target);
            }
        }
    }

    isValid(target, material) {
        if (!target || !target.isMesh) {
            console.error("ModelCustomizer: the target is not a valid mesh");
            return false;
        }

        if (!target.material && !material) {
            console.error("ModelCustomizer: the target does not have any material");
            return false;
        }

        if (this.hasSkeletonModifiers && !target.isSkinnedMesh) {
            console.error("ModelCustomizer: the target is not a skinned mesh");
            return false;
        }

        if (this.hasTextureModifiers && target.material.isMultiMaterial && !material) {
            console.warn("ModelCustomizer: the target has multiple materials, you may need " +
                "to specify manually which one to use (by default the first one is used)");
        }

        return true;
    }

}
