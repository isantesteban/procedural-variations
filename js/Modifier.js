class Modifier {

    constructor() {
        Object.defineProperty(this, 'id', {
            value: Modifier.nextId,
            writable: false
        });

        this.name = "Modifier" + this.id;
        this.enabled = true;
    }

    static get nextId() {
        Modifier._nextId = (Modifier._nextId || 0) + 1;
        return Modifier._nextId;
    }

    apply(target) {
        console.warn(this.name + ': apply() method not implemented');
    }

}
