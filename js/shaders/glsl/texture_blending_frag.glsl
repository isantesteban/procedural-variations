varying vec2 vUv;

uniform sampler2D textures[NUM_TEXTURES];

void main() {
    vec4 diffuseColor = vec4(0.0);

    for (int i = 0; i < NUM_TEXTURES; i++) {
        vec4 texelColor = texture2D(textures[i], vUv);
        diffuseColor = diffuseColor + texelColor;
    }

    gl_FragColor = clamp(diffuseColor, 0.0, 1.0);
}