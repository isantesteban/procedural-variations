varying vec2 vUv;

uniform sampler2D texture;

#ifdef USE_MASK
uniform sampler2D mask;
#endif

uniform float brightness;
uniform float contrast;
uniform float hue;
uniform float saturation;
uniform float value;

// Fast branchless RGB to HSV conversion in GLSL
// Author: Sam Hocevar

vec3 rgb2hsv(vec3 c) {
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;

    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);

    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
    vec4 fragColor = vec4(1.0);

    vec3 texelColor = texture2D(texture, vUv).rgb;

    vec3 hsvColor = rgb2hsv(texelColor);

    hsvColor[0] = clamp(hsvColor[0] + hue, -1.0, 1.0); // Hue
    hsvColor[1] = clamp(hsvColor[1] + saturation, 0.0, 1.0); // Saturation
    hsvColor[2] = clamp(hsvColor[2] + value, 0.0, 1.0); // Value

    fragColor.rgb = hsv2rgb(hsvColor);

    // Apply contrast
    fragColor.rgb = ((fragColor.rgb - 0.5) * max(contrast, 0.0)) + 0.5;

    // Apply brightness
    fragColor.rgb = fragColor.rgb + brightness;

    #ifdef USE_MASK
    float strength = texture2D(mask, vUv).r;
    fragColor.rgb = ((1.0 - strength) * texelColor) + (strength * fragColor.rgb);
    #endif

    gl_FragColor = fragColor;
}