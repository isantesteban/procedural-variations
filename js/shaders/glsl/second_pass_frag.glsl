varying vec2 vUv;

uniform sampler2D map;
uniform float texelOffset;

void main() {
	vec4 fragColor = texture2D(map, vUv);

    int numNeighbours = 0;

    if (fragColor.a < 1.0) {

        fragColor = vec4(0.0);

        for (int i = -1; i <= 1; i++){
            for (int j = -1; j <= 1; j++) {
                vec2 texelCoord = clamp(vUv + vec2(i,j)*texelOffset, 0.0, 1.0);
                vec4 texelColor = texture2D(map, texelCoord);

                if (texelColor.a == 1.0) {
                    fragColor += texelColor;
                    numNeighbours++;
                }
            }
        }

        if (numNeighbours > 0) fragColor = fragColor / float(numNeighbours);
    }

    gl_FragColor = fragColor;
}